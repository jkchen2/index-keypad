# Valve Index DIY Frunk Keypad

This is a little project to build a keypad accessory for the Valve Index frunk.

Documentation: https://jkchen2.gitlab.io/index-keypad

![static_wireframe](docs/media/static_wireframe.png)
![showcase](docs/media/showcase.webp)
![installed_front](docs/media/installed_front.webp)
![installed_side](docs/media/installed_side.webp)
