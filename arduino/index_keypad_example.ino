/*
 * index_keypad_example.ino
 *
 * This is the example file that showcases my own setup.
 * From top left to bottom right, the keys when pressed will:
 *
 * Activate Shadowplay (alt + f1)      | (no action)                        | Volume up
 * Windows Dictation (win + h)         | Discord mute (f14)                 | Volume down
 * 
 * And when held, will:
 * 
 * Shadowplay record (alt + ctrl + f1) | Toggle keypad lock                 | (no action)
 * Windows key (win)                   | Discord deafen (f15)               | Volume mute
 *
 * This sketch reads keypad button presses and simulates a keyboard keypress
 *   (or any other action as specified in press_state_changed)
 *
 * Assuming the keypad has been constructed with the given suggestions on the guide,
 *   the only things that you need to customize are hold_durations (if desired),
 *   and press_state_changed (to set which keypresses are simulated).
 */

#include "TrinketHidCombo.h"

// Analog read values between 0 and 1024 (0v and 5v)
// These default values assume all resistors used have the same resistance,
//   and that an extra resistor is used for the bottom row (to prevent resetting).
//   If you aren't mixing resistor values, you shouldn't have to modify this.
// Values map from the top left button to the bottom right button in rows.
const int button_values[2][3] = {
    {683, 512, 0},
    {768, 683, 512}
};
const int value_threshold = 15;

// Duration in ms to wait before a button counts as being held down,
//   and press_state_changed is fired with held == true.
// Leave at 0 to disable the hold trigger.
// Values map from the top left button to the bottom right button in rows.
const unsigned long hold_durations[2][3] = {
    {1000, 1000, 0},
    {1000, 1000, 1000}
};

// Duration in ms to wait before reading a value to debounce presses
const unsigned long debounce_duration = 20;
const unsigned long stabilize_debounce_duration = 20;

// If using the keypad lock feature, set this button to be the lock button
//   (otherwise it will not be possible to unlock the keypad)
// This is a {row, column} value. Remember that these are 0 indexed.
const int lock_button[2] = {0, 1};

// Button pins. Should be left untouched unless you know what you're doing
const int pins[2] = {A1, A0};

// State variables
unsigned long debounce_times[2] = {0, 0};
bool press_debounces[2] = {false, false};
unsigned long hold_times[2] = {0, 0};
int last_presses[2] = {-1, -1};
bool last_holds[2] = {false, false};
bool locked = false;

// Key helper caches
struct KHCombo {
    int modifiers;
    int keycode;
    bool operator==(const KHCombo &other) const {
        return this->modifiers == other.modifiers && this->keycode == other.keycode;
    }
    bool operator!=(const KHCombo &other) const {
        return !(*this == other);
    }
    bool is_empty() const {
        return (this->modifiers == 0 && this->keycode == 0);
    }
};
const int kh_cache_size = 5;
KHCombo kh_combos[2][3];
KHCombo kh_cache[kh_cache_size];

// This function is called any time the button state changes (not pressed -> pressed -> held)
void press_state_changed(int row, int button, bool pressed, bool held) {
    digitalWrite(1, pressed ? HIGH : LOW);
    if (row == 0) { // Top row

        if (button == 0) { // Left
            if (pressed) {
                if (held) { // Toggle Shadowplay recording
                    TrinketHidCombo.pressKey(
                        KEYCODE_MOD_LEFT_ALT | KEYCODE_MOD_LEFT_CONTROL, KEYCODE_F1
                    );
                    TrinketHidCombo.pressKey(0, 0);
                }
            } else if (!held) { // Activate Shadowplay
                TrinketHidCombo.pressKey(KEYCODE_MOD_LEFT_ALT, KEYCODE_F1);
                TrinketHidCombo.pressKey(0, 0);
            }

        } else if (button == 1) { // Center
            if (pressed && held) { // Toggle keypad lock
                set_locked(!locked);
            }

        } else if (button == 2) { // Right
            if (pressed) { // Volume up
                TrinketHidCombo.pressMultimediaKey(MMKEY_VOL_UP);
            }
        }

    }

    else if (row == 1) { // Bottom row

        if (button == 0) { // Left
            if (pressed) {
                if (held) { // Raw super key
                    TrinketHidCombo.pressKey(KEYCODE_MOD_LEFT_GUI, 0);
                    TrinketHidCombo.pressKey(0, 0);
                }
            } else if (!held) { // Windows dictation
                TrinketHidCombo.pressKey(KEYCODE_MOD_LEFT_GUI, KEYCODE_H);
                TrinketHidCombo.pressKey(0, 0);
            }

        } else if (button == 1) { // Center
            if (pressed) {
                if (held) { // Toggle Discord deafen
                    TrinketHidCombo.pressKey(0, 0x6A); // F15
                    TrinketHidCombo.pressKey(0, 0);
                }
            } else if (!held) { // Toggle discord mute
                TrinketHidCombo.pressKey(0, 0x69); // F14
                TrinketHidCombo.pressKey(0, 0);
            }

        } else if (button == 2) { // Right
            if (pressed) {
                if (held) { // Mute
                    TrinketHidCombo.pressMultimediaKey(MMKEY_MUTE);
                }
            } else if (!held) { // Volume down
                TrinketHidCombo.pressMultimediaKey(MMKEY_VOL_DOWN);
            }
        }

    }
}

// A helper function to simulate a press and hold on a key until it is released.
// Useful for simulating actual USB keypad behavior.
void key_helper(int row, int button, bool pressed, int modifiers, int keycode) {

    // Press combo -- add to list of combos
    if (pressed) {
        KHCombo new_combo = (KHCombo) {modifiers, keycode};
        if (!new_combo.is_empty()) {
            int current_index = 0;
            for (; current_index < kh_cache_size; current_index++) {
                if (kh_cache[current_index].is_empty()) {
                    kh_cache[current_index] = new_combo;
                    break;
                }
            }
            if (current_index < kh_cache_size) { // Added to list of combos
                kh_combos[row][button] = new_combo;
            }
        }

    // Release combo -- remove from list of combos
    } else {
        KHCombo old_combo = kh_combos[row][button];
        for (int current_index = 0; current_index < kh_cache_size; current_index++) {
            if (kh_cache[current_index] == old_combo) {
                kh_cache[current_index] = kh_combos[row][button] = (KHCombo) {0, 0};
                break;
            }
        }
    }

    uint8_t combined_modifiers = 0;
    uint8_t keycodes[kh_cache_size];
    for (int it = 0; it < kh_cache_size; it++) {
        combined_modifiers |= (uint8_t) kh_cache[it].modifiers;
        keycodes[it] = (uint8_t) kh_cache[it].keycode;
    }
    TrinketHidCombo.pressKeys(combined_modifiers, keycodes, kh_cache_size);
}

// A helper function for setting the lock state.
// If lock is enabled, no buttons will work.
void set_locked(bool state) {
    locked = state;
    digitalWrite(1, LOW);
    delay(100);
    if (locked) { // "lock": long single flash
        digitalWrite(1, HIGH);
        delay(500);
        digitalWrite(1, LOW);
    } else { // "unlock": 2 short flashes
        digitalWrite(1, HIGH);
        delay(200);
        digitalWrite(1, LOW);
        delay(200);
        digitalWrite(1, HIGH);
        delay(200);
        digitalWrite(1, LOW);
    }
    delay(500);
}

void setup()
{
    pinMode(1, OUTPUT);
    TrinketHidCombo.begin();
}

void loop()
{
    unsigned long current_time = millis();

    for (int row_index = 0; row_index < 2; row_index++) {

        // Debounce
        unsigned long debounce_time = debounce_times[row_index];
        if (debounce_time != 0 && current_time < debounce_time) {
            continue;
        }
        debounce_times[row_index] = 0;
        
        // Read values
        int current_button_value = analogRead(pins[row_index]);

        // Check to see if a button is pressed
        const int *row_button_values = button_values[row_index];
        int current_press = -1;
        for (int press_index = 0; press_index < 3; press_index++) {
            int button_value = row_button_values[press_index];
            if (abs(button_value - current_button_value) < value_threshold) {
                current_press = press_index;
                break;
            }
        }

        // Ignore presses if the keypad is locked
        if (
            current_press > -1 && locked
            && !(row_index == lock_button[0] && current_press == lock_button[1])
        ) {
            for (int it = 0; it < 10; it++) {
                digitalWrite(1, HIGH);
                delay(50);
                digitalWrite(1, LOW);
                delay(50);
            }
            delay(250);
            continue;
        }

        int last_press = last_presses[row_index];
        bool last_held = last_holds[row_index];
        unsigned long hold_time = hold_times[row_index];
        // Check if a different button is pressed, and the previous one should be released
        if (current_press != last_press) {

            // Press stabilize debounce because sometimes the values are off by a few ms
            if (press_debounces[row_index]) {
                press_debounces[row_index] = false;

                // Release debounce
                debounce_times[row_index] = current_time + debounce_duration;
                if (last_press > -1) { // Release the last pressed key
                    last_presses[row_index] = -1;
                    press_state_changed(row_index, last_press, false, last_held);
                }
                if (current_press > -1) { // Add currently pressed key to state
                    unsigned long hold_duration = hold_durations[row_index][current_press];
                    hold_times[row_index] = current_time + hold_duration;
                    last_presses[row_index] = current_press;
                    bool current_held = hold_duration == 0;
                    last_holds[row_index] = current_held;
                    press_state_changed(row_index, current_press, true, current_held);
                }

            } else {
                press_debounces[row_index] = true;
                debounce_times[row_index] = current_time + stabilize_debounce_duration;
            }

        // Check for holds
        } else if (current_press > -1 && !last_held && current_time >= hold_time) {
            last_holds[row_index] = true;
            press_state_changed(row_index, current_press, true, true);

        }

    }

    TrinketHidCombo.poll();
}
