# Installation

## Installing

To install the keypad into the Valve Index's frunk, first remove the visor covering the frunk.

Then, drop keypad into the frunk.
The vent slot mount will catch on the top edge of the frunk,
while the right side of the keypad should drop in.
This results in the USB connector sitting at an angle pointing slightly down.

Slide the keypad to the right as far as it can go.
The USB connector should slide into the port,
and your computer should recognize that a USB device was inserted.
The left edge of the keypad should still be raised.

Push down firmly on the left edge until the vent slot mount clicks into a vent slot.
If it doesn't click in automatically,
you may need to press up against the mounting switch to ensure the vent slot mount
is inserted into the vent slot.

Shown below is a picture of the mounting switch in the up (locked) position:

![lock_switch](media/lock_switch.webp)

## Removing

To remove the unit from the frunk,
press down on the mounting switch (relative to you facing the frunk from the front).
Keeping the switch held down, lift the left side of the keypad towards you until the
vent slot mount is no longer inserted into the vent slot.

Finally, push the right side of the keypad to the left until it is disconnected from the
USB port, at which point you can lift the keypad from the frunk to remove it.
