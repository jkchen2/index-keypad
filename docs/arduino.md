# Arduino

## Prerequisites

Before doing anything involving writing or modifying code for the keypad,
ensure that you follow the instructions for uploading a test sketch to your Digispark.

This likely involves adding an additional board manager URL from Digistump,
`https://digistump.com/package_digistump_index.json`.

As a smoke test, it is recommended that you upload a basic blink sketch.

## Requirements

The keypad code requires that a library be installed, `TrinketHidCombo`.
This can be found [here](https://github.com/adafruit/Adafruit-Trinket-USB){target=_blank}.

To install the library, download or clone the repository,
then compress the contents of `TrinketHidCombo` and name it `TrinketHidCombo.zip`.

In the Arduino IDE, go to Sketch -> Include Library -> Add .ZIP Library... ->
Select `TrinketHidCombo.zip`.

## Uploading

Download the [`index_keypad.ino`](https://gitlab.com/jkchen2/index-keypad/-/raw/main/arduino/index_keypad.ino?inline=false){target=_blank}
file and open it in the Arduino IDE.

### Testing

Without modifying the base code, upload the sketch to your Digispark.
Open a text editor and press each button. From the top left to the bottom right button,
each button press should produce a number 1-6 corresponding to their button number.

Alternatively, use a [keyboard checker](https://keyboardchecker.com/){target=_blank} to see
if keys are being pressed correctly.

## Configuration

To change which keys are pressed, modify the code in the function `press_state_changed`.

By default, each key presses their corresponding number key.
However, let's say we want to change it so that the top row buttons press
++f1++, ++f2++, and ++f3++ respectively, while the bottom row presses 
++shift+f1++, ++shift+f2++, and ++shift+f3++.
To do that, we can simply modify the function like so:

```arduino
void press_state_changed(int row, int button, bool pressed, bool held) {
    digitalWrite(1, pressed ? HIGH : LOW);
    if (row == 0) { // Top row

        if (button == 0) { // Left
            key_helper(row, button, pressed, 0, KEYCODE_F1);
        } else if (button == 1) { // Center
            key_helper(row, button, pressed, 0, KEYCODE_F2);
        } else if (button == 2) { // Right
            key_helper(row, button, pressed, 0, KEYCODE_F3);
        }

    }

    else if (row == 1) { // Bottom row

        if (button == 0) { // Left
            key_helper(row, button, pressed, KEYCODE_MOD_LEFT_SHIFT, KEYCODE_F1);
        } else if (button == 1) { // Center
            key_helper(row, button, pressed, KEYCODE_MOD_LEFT_SHIFT, KEYCODE_F2);
        } else if (button == 2) { // Right
            key_helper(row, button, pressed, KEYCODE_MOD_LEFT_SHIFT, KEYCODE_F3);
        }

    }
}
```

Here's another example, this time with media keys:

```arduino
void press_state_changed(int row, int button, bool pressed, bool held) {
    digitalWrite(1, pressed ? HIGH : LOW);
    if (row == 0) { // Top row

        if (button == 0) { // Left
            if (pressed) { // Volume up
                TrinketHidCombo.pressMultimediaKey(MMKEY_VOL_UP);
            }
        } else if (button == 1) { // Center
            // Press the print screen button
            key_helper(row, button, pressed, 0, KEYCODE_PRINTSCREEN);
        } else if (button == 2) { // Right
            // Press the F13 key
            key_helper(row, button, pressed, 0, 0x68);
        }

    }

    else if (row == 1) { // Bottom row

        if (button == 0) { // Left
            if (pressed) { // Volume down
                TrinketHidCombo.pressMultimediaKey(MMKEY_VOL_DOWN);
            }
        } else if (button == 1) { // Center
            if (pressed) { // Play or pause current media
                TrinketHidCombo.pressMultimediaKey(MMKEY_PLAYPAUSE);
            }
        } else if (button == 2) { // Right
            // Press the Windows key
            key_helper(row, button, pressed, KEYCODE_MOD_LEFT_GUI, 0);
        }

    }
}
```

To see a list of keycodes,
[click here](https://github.com/adafruit/Adafruit-Trinket-USB/blob/51f208bc569dd79d7357f16d548ed0c43f4927e8/TrinketHidCombo/TrinketHidCombo.h){target=_blank}.

To see a list of extra function keys (F13-F24, useful for custom keymaps in Discord and whatnot),
[click here](https://gist.github.com/MightyPork/6da26e382a7ad91b5496ee55fdc73db2#file-usb_hid_keys-h-L147-L158){target=_blank}.

## Advanced configuration

See the [`index_keypad_example.ino`](https://gitlab.com/jkchen2/index-keypad/-/raw/main/arduino/index_keypad_example.ino?inline=false){target=_blank}
file to see the more advanced use cases,
including functionality for locking the keypad against accidental presses,
alternative behavior for held keys,
and using `TrinketHidCombo.pressKey` directly.

When referencing this example, be mindful of the `hold_durations` variable,
which defines which keys support hold actions.

