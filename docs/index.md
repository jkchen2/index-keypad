# Valve Index DIY Frunk Keypad

This is a little project to build a keypad accessory for the Valve Index frunk.

If you're interested in seeing the build instructions, click on the links to the left.

To view the GitLab repository, click [here](https://gitlab.com/jkchen2/index-keypad){target=_blank}.

![showcase](media/showcase.webp)
![installed_front](media/installed_front.webp)
![installed_side](media/installed_side.webp)

<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>
<model-viewer src="media/keypad_model.glb" alt="Default assembled keypad model" auto-rotate camera-controls></model-viewer>
<model-viewer src="media/keypad_model_exploded.glb" alt="Exploded assembled keypad model" camera-controls>
    <button class="Hotspot" slot="hotspot-1" data-position="-77.63123321533203m 5.540806644292758m 0.33849956705178674m" data-normal="-1m 0m 0m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Mounting switch</div>
    </button><button class="Hotspot" slot="hotspot-3" data-position="-71.09269945752024m 7.240679623123782m -20.722794049338738m" data-normal="0m -0.5752113736758743m -0.8180048139185453m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Vent slot mount</div>
    </button><button class="Hotspot" slot="hotspot-4" data-position="-8.793758621761839m 5.444696599452676m 1.433931412370256m" data-normal="0m 0.08715571769162124m 0.9961947002838645m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Digispark MCU with headers</div>
    </button><button class="Hotspot" slot="hotspot-5" data-position="-71.16083004086607m 66.76792269873285m 16.86085538730481m" data-normal="0m 0.5690237199167221m 0.822321108918004m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Cherry MX switch</div>
    </button><button class="Hotspot" slot="hotspot-6" data-position="-73.28880614122497m 97.22663507511083m 18.123988979475186m" data-normal="-0.8973480078211743m 0.28941553605956294m 0.33317442931102254m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Cherry MX keycap</div>
    </button><button class="Hotspot" slot="hotspot-7" data-position="-54.10951156201999m -28.000000366063773m -0.7650817823519469m" data-normal="-0.000008022818436230726m -0.9999999999666267m -0.0000015430415101431103m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">M2 10 mm socket head screw</div>
    </button><button class="Hotspot" slot="hotspot-8" data-position="-7.509772645675973m 39.000000000377085m -0.3839257436157184m" data-normal="0m 1m -9.76507763529767e-12m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Indicator LED hole</div>
    </button>
</model-viewer>

Model sources:

* [Keycaps](https://grabcad.com/library/cherry-mx-keycap-1){target=_blank}
* [Switches](https://grabcad.com/library/cherry-mx-1){target=_blank}
* [Digispark](https://grabcad.com/library/digispark-attiny85-1){target=_blank}
* [Screws](https://www.mcmaster.com/screws/socket-head-screws){target=_blank}

<style>
model-viewer {
    height: 75vh;
    width: 100%;
}
.HotspotAnnotation {
    color: white;
}
</style>
