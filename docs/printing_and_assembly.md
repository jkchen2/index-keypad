# Printing and Assembly

## Printing

You will need to print the case enclosure and cover.

You can choose to either print the cover with or without the hole for the indicator LED.
The hole simply allows you to view the Digispark's on-board LED,
which flashes when keypad buttons are pressed.

Models can be found on GitLab:

* [case.stl](https://gitlab.com/jkchen2/index-keypad/-/raw/main/models/case.stl){target=_blank}
* [cover.stl](https://gitlab.com/jkchen2/index-keypad/-/raw/main/models/cover.stl){target=_blank}
* [cover_led.stl](https://gitlab.com/jkchen2/index-keypad/-/raw/main/models/cover_led.stl){target=_blank} (includes hole for the indicator light)

It is suggested you print these with PLA at a layer height of 0.20 mm.
PLA provides the right amount of flexibility to allow the mounting mechanism to function properly.

!!! note

    If you do not want to buy keycaps for the keypad, you can also print your own.
    I've only had limited success with this though, so YMMV.

!!! warning

    Before printing, make sure your printer can bridge up to 20 mm without issues.
    This should not be a problem for properly configured printers.

![slicer_case](media/slicer_case.webp)
![slicer_cover](media/slicer_cover.webp)

!!! note

    In the interest of saving time, you can print the cover first,
    then move on to the assembly step while you wait for the case to print.

### Testing the print

![unassembled_case](media/unassembled_case.webp)

Once the print is finished, test the case mounting mechanism by ensuring that the mounting lock
can move freely back and forth.
If it's seemingly stuck, it may require some pushing in a few directions as the
0.25 mm tolerance between the lower support and the flexible arm may have resulted in the two
slightly fusing together during the printing process.

If the mounting mechanism is completely stuck, you may need to re-print the part.
Check to make sure your printer is not overextruding filament,
and that there isn't any significant warping going on.

Once the mounting mechanism has been confirmed to move freely,
check to confirm that your Digispark fits in the designated space.
It should fit snugly, but not so tight that it cannot be removed.

![mcu_placement](media/mcu_placement.webp)

## Assembly

Assembly is done in multiple stages.
You can begin once the cover has finished printing, and do the wiring while the case is printing.

### Switch mounting

Once the cover is printed, you can begin by attaching the switches to the cover like so:

![plate_buttons_front](media/plate_buttons_front.webp)
![plate_buttons_back](media/plate_buttons_back.webp)

!!! note "Switch orientation"

    The switches are oriented as shown above as a suggestion.
    This arrangement allows for slightly neater wiring as can be seen in the following step.

### Wiring

!!! note "Test first"

    If this is your first time using your Digispark,
    ___I highly recommend testing the circuit on a breadboard first.___
    This ensures that the resistors you're using will work as well as your MCU.

!!! note

    Before starting, make sure that the header pins are soldered to your Digispark.

Wiring can be a bit tricky given the space available.
I recommend giving yourself a decent amount of slack to
reposition the wires once the cover is attached to the case.

Below is an awkward circuit diagram that illustrates which component goes where:

![circuit_diagram](media/circuit_diagram.webp)

And some pictures of the exposed wiring
(note that in the first image the cover is flipped upside-down,
so the top row is actually the bottom row):

![wiring_buttons](media/wiring_buttons.webp)
![wiring_resistor](media/wiring_resistor.webp)
![wiring_mcu](media/wiring_mcu.webp)

!!! note "Miscellaneous notes"

    * You may want to wrap the resistors in the second image above with electrical tape.
    * The hook up wires are soldered to the Digispark header pins upside-down because there is
      very little clearance above the pins when the case is attached.
    * This circuit should account for Digispark clones that have pin 5 designated as a reset pin.
      If you have an official Digispark, or a clone that doesn't reset when P5 is pulled to ground,
      you can theoretically exclude R7 in the circuit diagram and bridge ground between the two
      rows of switches directly. However, this is not advised.
    * Keen observers may notice that the pictures above depict 10k Ohm resistors.
      Turns out using these resistors didn't work out very well for me,
      so I still recommend using just 1k Ohm resistors instead.

### Attaching the case

If you printed the cover with the LED indicator hole,
you can cut up a small piece of square white paper and tape it to the underside of the hole.
This should be sufficient in diffusing the red LED of the Digispark.

When ready, insert the Digispark into the mount in the case by sliding the USB A connector through
the 20 mm wide hole, then pushing down the Digispark into the square mount
until it is held snugly in place (see the [testing section](#testing-the-print)).

Then, close the cover over the case being careful to keep wires out of the way of the screw holes
and indicator LED.

Holding the cover and the case tightly together,
turn the unit over and screw in the 4 machine screws.
Note that you should not over-tighten the screws.

Once the cover has been screwed into the case, feel free to attach the keycaps.

At this point, your keypad should be fully assembled:

![assembled_top](media/assembled_top.webp)

<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>
<model-viewer src="../media/keypad_model_exploded.glb" alt="Exploded assembled keypad model" camera-controls>
    <button class="Hotspot" slot="hotspot-1" data-position="-77.63123321533203m 5.540806644292758m 0.33849956705178674m" data-normal="-1m 0m 0m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Mounting switch</div>
    </button><button class="Hotspot" slot="hotspot-3" data-position="-71.09269945752024m 7.240679623123782m -20.722794049338738m" data-normal="0m -0.5752113736758743m -0.8180048139185453m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Vent slot mount</div>
    </button><button class="Hotspot" slot="hotspot-4" data-position="-8.793758621761839m 5.444696599452676m 1.433931412370256m" data-normal="0m 0.08715571769162124m 0.9961947002838645m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Digispark MCU with headers</div>
    </button><button class="Hotspot" slot="hotspot-5" data-position="-71.16083004086607m 66.76792269873285m 16.86085538730481m" data-normal="0m 0.5690237199167221m 0.822321108918004m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Cherry MX switch</div>
    </button><button class="Hotspot" slot="hotspot-6" data-position="-73.28880614122497m 97.22663507511083m 18.123988979475186m" data-normal="-0.8973480078211743m 0.28941553605956294m 0.33317442931102254m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Cherry MX keycap</div>
    </button><button class="Hotspot" slot="hotspot-7" data-position="-54.10951156201999m -28.000000366063773m -0.7650817823519469m" data-normal="-0.000008022818436230726m -0.9999999999666267m -0.0000015430415101431103m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">M2 10 mm socket head screw</div>
    </button><button class="Hotspot" slot="hotspot-8" data-position="-7.509772645675973m 39.000000000377085m -0.3839257436157184m" data-normal="0m 1m -9.76507763529767e-12m" data-visibility-attribute="visible">
        <div class="HotspotAnnotation">Indicator LED hole</div>
    </button>
</model-viewer>

<style>
model-viewer {
    height: 75vh;
    width: 100%;
}
.HotspotAnnotation {
    color: white;
}
</style>
