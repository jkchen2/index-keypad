# Parts and Tools

Before you begin, make sure you have the parts and tools necessary.

![parts](media/parts.webp)

## Parts

* Digispark microcontroller with headers (clones can also work)
    * [Example](https://smile.amazon.com/gp/product/B0836WXQQR){target=_blank}
* 7 resistors with the same resistance (1k Ohms tested and recommended)
    * [Example](https://www.adafruit.com/product/4294){target=_blank}
* 6 Cherry MX switches (clones can also work, ex. Gateron switches)
    * [Example](https://novelkeys.xyz/collections/switches/products/cherry-switches?variant=13587121111133){target=_blank}
* 6 Cherry MX compatible keycaps
    * [Example](https://smile.amazon.com/dp/B06XJSW4BF){target=_blank}
* 4 M2 10 mm socket head machine screws
    * [Example](https://smile.amazon.com/gp/product/B07FLLGW19){target=_blank}
* Assorted wires (flexible 30 AWG hook up wire recommended)
    * [Example](https://smile.amazon.com/gp/product/B07T1MR9C4){target=_blank}
* Electrical tape (optional)
    * [Example](https://smile.amazon.com/Plymouth-Brand-General-Purpose-Electrical/dp/B07RWFFN8L){target=_blank}

!!! note "Buying multiple parts"

    When buying Digispark clone boards, consider buying multiple at once in the event
    that one is accidentally damaged in the building process.

    I also recommend buying a variety pack of many of these parts,
    namely the resistors, screws, and hook up wires.

## Tools

* FDM 3D printer (minimum build plate size 100x100x50 mm) + PLA filament
* Soldering iron + solder
* Wire cutter (and optionally stripper)
* Screwdriver or hex key for your M2 screws
* Computer that can flash Arduino-compatible microcontrollers
* Valve Index (optional)

!!! note

    Most popular 3D printers should be accurate enough for the build.
    For reference, I used a Creality Ender 3 and printed with PLA filament
    at the default 0.20 mm layer height.
